---
version: 2
type de projet: Travail de bachelor
année scolaire: 2020/2021
titre: FestiNeuch, application d’interaction et un tunnel d’immersion 
filières:
  - Informatique
  - Télécommunications
  - ISC
nombre d'étudiants: 1
mandants: 
  - Haute Ecole Arc, Gestion(He-arc) - Prof. Maria Sokhn
professeurs co-superviseurs:
  - Omar Abou Khaled
  - Karl Daher
mots-clés: [Sound, Music, Development]
langue: [F,E]
confidentialité: non
suite: non
---

```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/festineuch.png}
\end{center}
```

## Description/Contexte

Dans le cadre du festival de FestiNeuch, le festival de musique ayant lieu sur les rives du lac de Neuchâtel, nous allons développer une application d’interaction et d’immersion. L’application a pour but de partager l’histoire de FestiNeuch. Afin que l’application soit immersive nous travaillerons sur les interactions possibles avec le texte, le son, la lumière et les odeurs. Dans ce cadre l’application doit pouvoir créer un environnement festif en synchronisant le texte, la musique avec la lumière fournie par FestiNeuch et de deux, pouvoir en contrôlant des machines qui diffusent des odeurs.

En collaboration avec la He-arc nous travaillerons sur ce sujet pour développer le prototype. 


## Objectifs/Tâches
 
Tâches primaires :
  - Initiation et compréhension des services à développer
  - Recherche des technologies pour développer les différentes parties
  - Développement du service qui pourra synchroniser la musique et la lumière

Tâches Secondaires :
  - Test de la machine de diffusion d’odeur
  - Synchronisation de la machine avec les différentes étapes du scénario

