---
version: 2
type de projet: Travail de bachelor
année scolaire: 2020/2021
titre: Application pour l'autosoin de la maladie de Crohn
filières:
  - Informatique
  - ISC
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Omar Abou Khaled
  - Leonardo Angelini
proposé par étudiant: Loris Kenklies
mots-clés: [autosoins, Crohn, app, Flutter]
langue: [F,E]
confidentialité: non
suite: oui
---

## Contexte

Dans le cadre du projet de semestre 5 et 6, en collaboration avec des étudiants de la Haute Ecole de Santé, une application pour l'autosoin des la sclérose en plaques a été développée et sera testée avec des utilisateurs. Le but de ce projet est d'implementer une version améliorée de l'application existante, afin de prendre en compte les retours des utilisateurs, ainsi que de dévélopper un portail pour le personnel soignant.


## Objectifs
- Analyse des retours utilisateurs
- Conception et implementation d'une plateforme pour le personnel soignant
- Implementation des améliorations dans l'app
- Tests utilisateur 

## Contraintes
Suite du projet de semestre 5 en collaboration avec la HEdS
