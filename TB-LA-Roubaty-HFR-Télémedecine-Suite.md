---
version: 2
type de projet: Travail de bachelor
année scolaire: 2020/2021
titre: Algorithmes d'apprentissage automatique pour l'analyse des données des patients
filières:
  - Informatique
  - ISC
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Omar Abou Khaled
  - Leonardo Angelini
proposé par étudiant: Loris Roubaty
mots-clés: [télémedecine, HFR, machine learning]
langue: [F,E]
confidentialité: non
suite: oui
---

## Contexte

Dans le cadre des projets de semestre 5 et 6, en collaboration avec l'Hôpital Cantonal de Fribourg, une infrastructure web en microservices à été dévéloppée afin de pouvoir collecter les données des plusieurs dispositifs connectées intégrés dans la valise de télémedecine de l'HFR et pouvoir visualiser ces informations sur une interface web. Le but de ce projet est d'implementer un alogorithme d'apprentissage automatique afin d'analyser les données typiques recoltées dans un contexte hospitalier. 


## Objectifs
- Analyse des besoins des utilisateurs
- Analyse statistique des données
- Conception et implementation d'un algorithme pour la detection des phases de la maladie 
- Integration dans la plateforme existante
- Tests de l'algorithme

## Contraintes
Suite du projet de semestre 6 en collaboration avec l'HFR
