---
version: 2
type de projet: Projet de bachelor
année scolaire: 2020/2021
titre:  Application for children anti-bullying at school. 
filières:
  - Informatique
  - Télécommunications
  - ISC
nombre d'étudiants: 1

professeurs co-superviseurs:
  - Omar Abou Khaled
  - Mira El Kamali
  - Marine Capallera

mots-clés: [Unity, storytelling, scenario ]
langue: [F,E]
confidentialité: non
suite: non
---

## Description/Contexte

L'intimidation a toujours été un problème, en particulier avec les enfants à l'école, où un enfant peut intimider un autre enfant. Les enseignants ont également du mal à y faire face.
Afin de sensibiliser à la manière de lutter contre l'intimidation à l'école entre les enfants, nous souhaitons mettre en œuvre un scénario de narration dans lequel l'utilisateur peut participer en tant que spectacle intimidé / enseignant /intimidant et voir comment il va choisir les scènes de transitions. L'application peut être une application sur telephonne ou web.

## Objectifs/Tâches
 
- Chercher des Unity Models Gratuit
- Implémentation of the scenario 
    - Start of the expremient
    - Scene transition 
    - Dialogue, text interaction, response 
