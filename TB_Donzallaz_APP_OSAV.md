---
version: 2
type de projet: Travail de bachelor
année scolaire: 2020/2021
titre: Chatbot pour l'évaluation de la consommation d'emballages alimentaires et la recommandation de bonnes pratiques pour les citoyens suisses.
filières:
  - Informatique
  - ISC
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Omar ABOU KHALED 
  - Karl DAHER
proposé par étudiant: Joé Donzallaz
mots-clés: [Recommender system, IA, ML, NLP, chatbot]
langue: [F,E]
confidentialité: oui
suite: non
---

## Contexte
L'OSAV (Office fédéral de la sécurité alimentaire et des affaires vétérinaires) cherche à mesurer et à surveiller la consommation d'emballages alimentaires au sein de la population suisse. Jusqu'à présent, ces informations ont été obtenues en utilisant les méthodes d'interview classiques.
De nos jours, les chatbots, ou agents conversationnels, sont utilisés et déployés à de nombreuses fins comme le service client ou le coaching. Grâce aux progrès de la NLP, l'IA et le Machine Learning, les technologies permettant de développer des chatbots sont en plein essor. 
L'idée derrière ce projet est d'utiliser un agent conversationnel pour évaluer la population et lui donner des recommandations de bonnes pratiques.


## Objectifs
L'objectif de ce projet de bachelor est d'utiliser les techniques de pointe dans le développement de chatbots pour créer un agent capable d'évaluer et de mesurer la consommation alimentaire d'une personne qui lui parle et ensuite de lui fournir des recommandations sur les bonnes pratiques en vigueur. L'objectif est de développer une méthodologie de mesure capable de remplacer l'ancienne méthode pour cette tâche et de mettre un système de recommandation pour l'utilisateur.

Le travail sera divisé comme suit : Rédaction des spécifications et planification, Analyse, Conception, Implémentation, Test&Evaluation. Un prototype sera construit et testé. 


